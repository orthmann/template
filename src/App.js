import React from 'react';

function App({ token, metadata, error }) {
  const [data, setData] = React.useState(null);

  React.useEffect(() => {
    fetch('https://cloud-wallet.xfsc.dev/api/dynamic/plugins/plugin-template/application/app', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
    .then((res) => {
      if (!res.ok) {
        throw new Error(`Failed to fetch: ${res.status} ${res.statusText}`);
      }
      return res.json();
    })
    .then((data) => setData(data.title))
    .catch((err) => error(err));
  }, []);

  return (
    <div className="w-100 h-100 d-flex justify-content-center align-items-center flex-column">
      <h1>Fetched data bellow</h1>
      <h2>{!data ? 'Loading...' : data}</h2>
      <hr />
      <h2>Props bellow:</h2>
      {Object.keys(metadata).map((key) => {
        if (typeof metadata[key] !== 'string') return null;

        return (
          <div key={key}>
            <strong>{key}:</strong> {metadata[key]}
          </div>
        );
      })}
    </div>
  );
}

export default App;
